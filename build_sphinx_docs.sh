#!/bin/bash

# copy over example notebooks
cp notebooks/*.ipynb docs/source/
# build up from scratch, put in dummy dir to move out html in CI
python -m sphinx docs/source public -j4 -a
