TSC Docs
========

.. automodule:: tsc.__init__
    :members:

[extras] Extension Code
=======================

.. note::
    These functions will only work if their dependencies are installed with the ``[extras]`` version of the
    ``topological-signal-compression`` package, e.g. by installing as
    ``pip install topological-signal-compression[extras]``. There also some non-``pip`` dependencies that must be
    be installed to run ``opus``, discussed further in the docs for :py:func:`tsc.utils.compression.compress_opus()`.

Counterfactual Compression Pipelines
------------------------------------

.. automodule:: tsc.utils.compression_pipelines
    :members:

Counterfactual Compression Methods
----------------------------------

.. automodule:: tsc.utils.compression
    :members:

Counterfactual Reconstruction Methods
-------------------------------------

.. automodule:: tsc.utils.reconstruction
    :members:

Data Loaders
------------

.. automodule:: tsc.utils.data
    :members:

Helper Functions
----------------

.. automodule:: tsc.utils.helper_functions
    :members:
