#!/bin/env python3

# Gary Koplik
# gary<dot>koplik<at>geomdata<dot>com
# May, 2022
# extras_dependencies.py

"""
Reads off the ``[extras]`` dependencies from ``pyproject.toml`` so they can be ``pip`` installed in a CI/CD job.
"""

import toml
import pathlib
pyproject_path = pathlib.Path("pyproject.toml")

print(" ".join(toml.load(pyproject_path.open())["project"]["optional-dependencies"]["extras"]))
