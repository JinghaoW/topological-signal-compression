#!/bin/env python3

# Gary Koplik
# gary<dot>koplik<at>geomdata<dot>com
# May, 2022
# get_version.py

"""
Reads off the current version from ``pyproject.toml`` so that version can be ``pip`` installed in a CI/CD job.
"""

import toml
import pathlib
pyproject_path = pathlib.Path("pyproject.toml")

print(toml.load(pyproject_path.open())["project"]["version"])
