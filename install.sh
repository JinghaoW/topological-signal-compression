#!/bin/bash
mamba env create -f tsc.yml
source activate tsc
pip install -e .[extras,testing]
python -m ipykernel install --user --name tsc --display-name "Python (tsc)"
