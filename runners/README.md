# runners

Python scripts for generating outputs. All meant to be run from the
root of the repository:

```{bash}
$ cd <path/to/topological-signal-compression>
$ bash install.sh
$ conda activate tsc
$ python ./runners/<runner_name>.py
```