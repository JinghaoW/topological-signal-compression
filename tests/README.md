# ./tests

All tests (and test setup) goes in here.

Note: tests are designed to be run from the *root* of the repo. See the "Testing" section of the top-level
`README.md` for more on how to run these tests.