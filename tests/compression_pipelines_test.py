# Gary Koplik, Sam Voisin
# gary<dot>koplik<at>geomdata<dot>com
# March, 2021. Last modified April, 2022.
# compression_utils_test.py


"""
Tests for non-TSC compression pipelines (and TSC against FSDD data).
"""

import numpy as np
import os
import pytest
from tsc.utils.compression_pipelines import dft_pipeline, opus_pipeline, paa_pipeline, random_pipeline
from tsc import tsc_pipeline
from tsc.utils.data import fsdd


@pytest.fixture(scope="class")
def init_pipeline_outputs(request):
    request.cls.test_input = np.array([1, -1, 5, -3, -2])
    request.cls.rnd_result = np.array([1, -1, -2, -3, -2])
    request.cls.paa_result = np.array([-0.25,  0.25,  0.75, -0.75, -4.25])
    yield


@pytest.fixture(scope="class")
def init_fsdd_example(request):
    request.cls.fsdd_input = fsdd(1)[0][0]
    request.cls.sampling_rate = 8000  # FSDD sampling rate
    yield


@pytest.mark.usefixtures("init_pipeline_outputs")
class TestPipelineFunctionsSpecificValues:
    """
    Tests for the more interpretable pipeline functions (e.g. not opus or dft) on getting some specific values out.
    """
    
    @pytest.mark.parametrize("n_keep", [4, 0.8])
    def test_random_pipeline(self, n_keep):
        """
        Make sure ``tsc.utils.compression_pipelines.random_pipeline()`` gives us the expected results for a toy example.
        """
        assert np.all(random_pipeline(signal=self.test_input,
                                      n_keep=n_keep,
                                      random_seed=1) == self.rnd_result)

    def test_paa_pipeline(self):
        """
        Make sure ``tsc.utils.compression_pipelines.paa_pipeline()`` gives us the expected results for a toy example.
        """
        assert np.all(paa_pipeline(signal=self.test_input,
                                   window_size=2) == self.paa_result)

    @pytest.mark.parametrize("func", [tsc_pipeline, random_pipeline])
    def test_pipelines_no_compression(self, func):
        """
        Make sure pipelines that can request compression that isn't compression at all (e.g. with ``n_keep``)
        return the original signal.
        """
        assert np.array_equal(func(signal=self.test_input, n_keep=1e6), self.test_input)

    def test_pipelines_no_compression_paa(self):
        """
        Make sure ``tsc.utils.compression_pipelines.paa_pipeline()`` can request compression that isn't compression
        at all (e.g. ``window_size=1`` returns the original signal).
        """
        assert np.array_equal(paa_pipeline(signal=self.test_input, window_size=1), self.test_input)


@pytest.mark.usefixtures("init_fsdd_example")
class TestPipelineFSDDExample:
    """
    Tests for all of the pipeline functions on a single FSDD example
    """
    def test_tsc_pipeline(self):
        """
        Make sure ``tsc.tsc_pipeline()`` behaves as expected for a more complicated example.
        """

        # run at higher compressions since there's a minimum compression of "number of critical points" for TSC
        out_low_compression = tsc_pipeline(signal=self.fsdd_input, n_keep=0.5)
        out_high_compression = tsc_pipeline(signal=self.fsdd_input, n_keep=0.2)

        # reconstructs to correct size
        assert out_low_compression.size == out_high_compression.size == self.fsdd_input.size

        # more compressed is more "wrong"
        assert np.abs(out_high_compression - self.fsdd_input).sum() \
               > np.abs(out_low_compression - self.fsdd_input).sum()

    def test_dft_pipeline(self):
        """
        Make sure ``tsc.utils.compression_pipelines.dft_pipeline()`` behaves as expected for a more complicated example.
        """

        out_low_compression = dft_pipeline(signal=self.fsdd_input, percent_compressed=20)
        out_high_compression = dft_pipeline(signal=self.fsdd_input, percent_compressed=50)

        # reconstructs to correct size
        assert out_low_compression.size == out_high_compression.size == self.fsdd_input.size

        # more compressed is more "wrong"
        assert np.abs(out_high_compression - self.fsdd_input).sum() \
               > np.abs(out_low_compression - self.fsdd_input).sum()

    def test_opus_pipeline(self):
        """
        Make sure ``tsc.utils.compression_pipelines.opus_pipeline()`` behaves as expected for a more complicated
        example.
        """

        log_file = "data/pytest/test_opus_pipeline_log.txt"

        less_compressed_wav = "data/pytest/test_opus_pipeline_less.wav"
        more_compressed_wav = "data/pytest/test_opus_pipeline_more.wav"

        # auto-generated `.opus` files will name match `.wav` files
        less_compressed_opus = os.path.join(os.path.dirname(less_compressed_wav),
                                            f"{os.path.basename(less_compressed_wav).split('.')[0]}.opus")
        more_compressed_opus = os.path.join(os.path.dirname(more_compressed_wav),
                                            f"{os.path.basename(more_compressed_wav).split('.')[0]}.opus")

        out_low_compression = opus_pipeline(signal=self.fsdd_input, wav_path=less_compressed_wav,
                                            bitrate=90, log_file=log_file, sampling_rate=self.sampling_rate)
        out_high_compression = opus_pipeline(signal=self.fsdd_input, wav_path=more_compressed_wav,
                                             bitrate=8, log_file=log_file, sampling_rate=self.sampling_rate)

        # reconstructs to correct size
        assert out_low_compression.size == out_high_compression.size == self.fsdd_input.size

        # more compressed is more "wrong"
        assert np.abs(out_high_compression - self.fsdd_input).sum() \
               > np.abs(out_low_compression - self.fsdd_input).sum()

        # kill the files on disk
        os.remove(less_compressed_wav)
        os.remove(less_compressed_opus)

        os.remove(more_compressed_wav)
        os.remove(more_compressed_opus)

        os.remove(log_file)

    def test_paa_pipeline(self):
        """
        Make sure ``tsc.utils.compression_pipelines.paa_pipeline()`` behaves as expected for a more complicated example.
        """

        out_low_compression = paa_pipeline(signal=self.fsdd_input, window_size=2)
        out_high_compression = paa_pipeline(signal=self.fsdd_input, window_size=10)

        # reconstructs to correct size
        assert out_low_compression.size == out_high_compression.size == self.fsdd_input.size

        # more compressed is more "wrong"
        assert np.abs(out_high_compression - self.fsdd_input).sum() \
               > np.abs(out_low_compression - self.fsdd_input).sum()

    def test_random_pipeline(self):
        """
        Make sure ``tsc.utils.compression_pipelines.random_pipeline()`` behaves as expected for a more complicated
        example.
        """

        out_low_compression = random_pipeline(signal=self.fsdd_input, n_keep=0.8, random_seed=1)
        out_high_compression = random_pipeline(signal=self.fsdd_input, n_keep=0.5, random_seed=1)

        # reconstructs to correct size
        assert out_low_compression.size == out_high_compression.size == self.fsdd_input.size

        # more compressed is more "wrong"
        assert np.abs(out_high_compression - self.fsdd_input).sum() \
               > np.abs(out_low_compression - self.fsdd_input).sum()
