# Gary Koplik
# gary<dot>koplik<at>geomdata<dot>com
# April, 2022
# compression_test.py

"""
Tests for ``tsc.utils.compression.py``.
"""


import pytest
from tsc.utils.compression import *
from tsc.utils.data import fsdd


@pytest.fixture(scope="module")
def init_fsdd_example():
    global fsdd_signal
    global sampling_rate

    fsdd_signal = fsdd(1)[0][0]
    sampling_rate = 8000  # FSDD sampling rate
    yield


@pytest.mark.usefixtures("init_fsdd_example")
def test_compress_dft():
    """
    Test that ``tsc.utils.compress_dft()`` behaves as expected.
    """

    less_compressed = compress_dft(signal=fsdd_signal, percent_compressed=10)
    more_compressed = compress_dft(signal=fsdd_signal, percent_compressed=50)

    assert more_compressed.size < less_compressed.size < fsdd_signal.size

    # should be same, just keeping different number of coefficients
    assert np.array_equal(more_compressed, less_compressed[:more_compressed.size])


@pytest.mark.usefixtures("init_fsdd_example")
def test_compress_opus():
    """
    Test that ``tsc.utils.compress_opus()`` behaves as expected.
    """
    log_file = "data/pytest/test_compress_opus_log.txt"

    less_compressed_wav = "data/pytest/test_compress_opus_less.wav"
    less_compressed_opus = compress_opus(signal=fsdd_signal, wav_path=less_compressed_wav,
                                         bitrate=90, log_file=log_file, sampling_rate=sampling_rate)

    more_compressed_wav = "data/pytest/test_compress_opus_more.wav"
    more_compressed_opus = compress_opus(signal=fsdd_signal, wav_path=more_compressed_wav,
                                         bitrate=8, log_file=log_file, sampling_rate=sampling_rate)

    assert os.stat(more_compressed_opus).st_size \
           < os.stat(less_compressed_opus).st_size \
           < os.stat(more_compressed_wav).st_size

    # kill the files on disk
    os.remove(less_compressed_wav)
    os.remove(less_compressed_opus)

    os.remove(more_compressed_wav)
    os.remove(more_compressed_opus)

    os.remove(log_file)


@pytest.mark.usefixtures("init_fsdd_example")
def test_compress_paa():
    """
    Test that ``tsc.utils.compress_paa()`` behaves as expected.
    """
    less_compressed = compress_paa(signal=fsdd_signal, window_size=2)
    more_compressed = compress_paa(signal=fsdd_signal, window_size=10)

    assert more_compressed.size < less_compressed.size < fsdd_signal.size


@pytest.mark.usefixtures("init_fsdd_example")
def test_compress_random():
    """
    Test that ``tsc.utils.compress_random()`` behaves as expected.
    """

    signal = np.c_[np.arange(fsdd_signal.size), fsdd_signal]

    # make sure setting seed works
    assert np.array_equal(compress_random(signal=signal,
                                          random_seed=0, num_indices_to_keep=signal.shape[0]//2),
                          compress_random(signal=signal,
                                          random_seed=0, num_indices_to_keep=signal.shape[0]//2))
    assert not np.array_equal(compress_random(signal=signal,
                                              random_seed=0, num_indices_to_keep=signal.shape[0]//2),
                              compress_random(signal=signal,
                                              random_seed=1, num_indices_to_keep=signal.shape[0]//2))

    # make sure "all" behaves as expected
    assert np.array_equal(signal, compress_random(signal=signal, num_indices_to_keep="all"))

    # include breaking cases for `num_indices_to_keep`
    for num_indices_to_keep in [-1, 1.5, 0, 1]:
        try:
            compress_random(signal=signal, num_indices_to_keep=num_indices_to_keep)
            assert False, "Invalid `num_indices_to_keep` param should've triggered error"
        except AssertionError:
            assert True

    less_compressed = compress_random(signal=signal, num_indices_to_keep=0.8)
    more_compressed = compress_random(signal=signal, num_indices_to_keep=0.1)

    assert more_compressed.shape[0] < less_compressed.shape[0] < signal.shape[0]
