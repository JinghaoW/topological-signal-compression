# Gary Koplik
# gary<dot>koplik<at>geomdata<dot>com
# April, 2022
# data_test.py


"""
Tests for ``tsc.utils.data.py``
"""


from tsc.utils.data import *


def test_fsdd():
    """
    Make sure ``tsc.utils.data.fsdd()`` behaves as expected.
    """

    num_examples = 10

    signals, labels = fsdd(dataset_size=num_examples)

    assert isinstance(signals[0], np.ndarray)
    assert isinstance(labels[0], int)

    assert len(signals) == len(labels) == num_examples

    signals, labels = fsdd(dataset_size=999999)

    # total number of valid examples we use
    assert len(signals) == len(labels) == 2816


def test_chopin():
    """
    Make sure ``tsc.utils.data.chopin()`` behaves as expected.
    """

    snippet = chopin()

    assert isinstance(snippet, AudioSegment)
    assert snippet.frame_rate == 44100
    assert snippet.duration_seconds == 10
