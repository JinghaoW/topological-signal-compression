# Gary Koplik
# gary<dot>koplik<at>geomdata<dot>com
# April, 2022
# reconstruction_test.py

"""
Tests for ``tsc.utils.reconstruction.py``.
"""


import pytest
from tsc.utils.compression import *
from tsc.utils.data import fsdd
from tsc.utils.reconstruction import *


@pytest.fixture(scope="module")
def init_fsdd_example():
    global fsdd_signal
    global sampling_rate

    fsdd_signal = fsdd(1)[0][0]
    sampling_rate = 8000  # FSDD sampling rate
    yield


@pytest.mark.usefixtures("init_fsdd_example")
def test_reconstruct_dft():
    """
    Test that ``tsc.utils.reconstruct_dft()`` behaves as expected.
    """

    # hit the odd-sized compression and even-sized reconstruction, should be similar
    compressed_odd = compress_dft(signal=fsdd_signal, percent_compressed=50)
    compressed_even = compress_dft(signal=fsdd_signal, percent_compressed=50.2)

    assert compressed_odd.size % 2 == 1
    assert compressed_even.size % 2 == 0

    reconstructed_odd = reconstruct_dft(fourier_coefficients=compressed_odd, size=fsdd_signal.size)
    reconstructed_even = reconstruct_dft(fourier_coefficients=compressed_even, size=fsdd_signal.size)
    assert np.allclose(reconstructed_odd, reconstructed_even, atol=2)

    # make sure compressions to different levels disrupt signal different amounts

    compressed_more = compress_dft(signal=fsdd_signal, percent_compressed=80)
    reconstructed_more = reconstruct_dft(fourier_coefficients=compressed_more, size=fsdd_signal.size)

    # reconstructs to correct size
    assert reconstructed_even.size == reconstructed_odd.size == reconstructed_more.size == fsdd_signal.size

    # more compressed is more "wrong"
    assert np.abs(reconstructed_more - fsdd_signal).sum() > np.abs(reconstructed_odd - fsdd_signal).sum()


def test_reconstruct_opus():
    """
    Test that ``tsc.utils.reconstruct_opus()`` behaves as expected.
    """
    log_file = "data/pytest/test_reconstruct_opus_log.txt"

    less_compressed_wav = "data/pytest/test_reconstruct_opus_less.wav"
    less_compressed_opus = compress_opus(signal=fsdd_signal, wav_path=less_compressed_wav,
                                         bitrate=90, log_file=log_file, sampling_rate=sampling_rate)

    more_compressed_wav = "data/pytest/test_reconstruct_opus_more.wav"
    more_compressed_opus = compress_opus(signal=fsdd_signal, wav_path=more_compressed_wav,
                                         bitrate=8, log_file=log_file, sampling_rate=sampling_rate)

    reconstructed_less_compressed = reconstruct_opus(filepath=less_compressed_opus, sampling_rate=sampling_rate)
    reconstructed_more_compressed = reconstruct_opus(filepath=more_compressed_opus, sampling_rate=sampling_rate)

    assert reconstructed_less_compressed.size == reconstructed_more_compressed.size == fsdd_signal.size

    # more compressed is more "wrong"
    assert np.abs(reconstructed_more_compressed - fsdd_signal).sum() \
           > np.abs(reconstructed_less_compressed - fsdd_signal).sum()

    # kill the files on disk
    os.remove(less_compressed_wav)
    os.remove(less_compressed_opus)

    os.remove(more_compressed_wav)
    os.remove(more_compressed_opus)

    os.remove(log_file)


def test_reconstruct_paa():
    """
    Test that ``tsc.utils.reconstruct_paa()`` behaves as expected.
    """
    less_window = 2
    less_compressed = compress_paa(signal=fsdd_signal, window_size=less_window)

    more_window = 10
    more_compressed = compress_paa(signal=fsdd_signal, window_size=more_window)

    reconstructed_less_compressed = reconstruct_paa(signal=less_compressed,
                                                    original_signal_size=fsdd_signal.size,
                                                    window_size=less_window)
    reconstructed_more_compressed = reconstruct_paa(signal=more_compressed,
                                                    original_signal_size=fsdd_signal.size,
                                                    window_size=more_window)

    assert reconstructed_less_compressed.size == reconstructed_more_compressed.size == fsdd_signal.size

    # more compressed is more "wrong"
    assert np.abs(reconstructed_more_compressed - fsdd_signal).sum() \
           > np.abs(reconstructed_less_compressed - fsdd_signal).sum()


def test_reconstruct_random():
    """
    Test that ``tsc.utils.reconstruct_random()`` behaves as expected.

    """

    to_compress = np.c_[np.arange(fsdd_signal.size), fsdd_signal]

    # make sure compressions to different levels disrupt signal different amounts
    compressed_less = compress_random(signal=to_compress, num_indices_to_keep=0.8, random_seed=0)
    reconstructed_less = reconstruct_random(signal=compressed_less)

    compressed_more = compress_random(signal=to_compress, num_indices_to_keep=0.4, random_seed=0)
    reconstructed_more = reconstruct_random(signal=compressed_more)

    # reconstructs to correct size
    assert reconstructed_less.shape[0] == reconstructed_more.shape[0] == fsdd_signal.size

    # more compressed is more "wrong"
    assert np.abs(reconstructed_more[:, 1] - fsdd_signal).sum() \
           > np.abs(reconstructed_less[:, 1] - fsdd_signal).sum()
