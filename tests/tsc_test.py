# Gary Koplik
# gary<dot>koplik<at>geomdata<dot>com
# February, 2020
# tsp_test.py

"""
Tests for ``src/tsc/__init__.py``
"""


import pytest
from tsc import *


def test_signal_persistence():
    """
    Make sure ``tsc.signal_persistence()`` works on a simple example as expected.
    """
    signal_1d = np.array([1, -1, 5, -3, -2])
    arr = np.c_[np.arange(signal_1d.size), signal_1d]

    pers_df = signal_persistence(arr)

    assert pers_df.to_dict() == {'birth_index': {0: 1, 1: 3},
                                 'death_index': {0: 2, 1: 2},
                                 'birth': {0: -1.0, 1: -3.0},
                                 'death': {0: 5.0, 1: 5.0},
                                 'pers': {0: 6.0, 1: 8.0}}

    return None


class TestCompressTSC:
    """
    Tests for ``tsc.compress_tsc()``.
    """

    @pytest.fixture(autouse=True)
    def setup(self):
        np.random.seed(27703)

        # two different positive / negative values
        choices = [-1, -0.5, 0.5, 1]

        # length of signal
        signal_length = 100

        # random walk plus a little bit of noise to separate values on pers diagram
        data = np.cumsum(np.random.choice(choices, signal_length)) + np.random.normal(0, 0.1, signal_length)
        times = np.arange(data.size)
        
        self.signal = np.c_[times, data]

        self.pers = signal_persistence(self.signal)

    def teardown(self):
        pass

    @pytest.mark.parametrize("pers_diag", ["calculate_actual_pers", "pers_subset",  None])
    @pytest.mark.parametrize("persistence_cutoff", [-np.inf, 0, 1, np.inf, None])
    @pytest.mark.parametrize("num_indices_to_keep", [0, 2, "all", None])
    def test_compress_tsc_run(self, pers_diag, persistence_cutoff, num_indices_to_keep):
        """
        Make sure `tsc.reconstruct_signal()` always runs or returns an error as expected.

        :param pers_diag: persistence diagram input.
        :param persistence_cutoff: cutoff value for critical pairs to keep when reconstructing signal.
        :param num_indices_to_keep: how many of the top critical pairs to keep when reconstructing signal.
        """
        try:
            if pers_diag == "calculate_actual_pers":
                pers = self.pers
            elif pers_diag == "pers_subset":
                pers = self.pers.iloc[:1, :]
            else:
                pers = None

            compress_tsc(self.signal, pers_diag=pers, persistence_cutoff=persistence_cutoff,
                         num_indices_to_keep=num_indices_to_keep)
            assert True
            return None

        except NotImplementedError:

            if persistence_cutoff is None and num_indices_to_keep is None:
                assert True
                return None

            elif persistence_cutoff is not None and num_indices_to_keep is not None:
                assert True
                return None

            else:
                assert False

        except AssertionError:
            assert True
            return None
            
    @pytest.mark.parametrize("num_indices_to_keep", [-1, -0.5, 0, 1])
    def test_compress_tsc_bad_num_indices_to_keep(self, num_indices_to_keep: float):
        """
        Make sure we hit an assertion error with inappropriate ``num_indices_to_keep`` values (must be >=2).
        """
        try:
            compress_tsc(self.signal, num_indices_to_keep=num_indices_to_keep)
            assert False
        except AssertionError:
            assert True

        return None

    @pytest.mark.parametrize("num_indices_to_keep", [0.5, 0.99, 2, 4])
    def test_compress_tsc_edges(self, num_indices_to_keep):
        """
        Make sure we keep the edges at many levels of reconstruction using ``num_indices_to_keep``, float or int.
        """
        sig = compress_tsc(self.signal, num_indices_to_keep=num_indices_to_keep)

        original_edges = np.take(self.signal, [0, -1], axis=0)
        reconstructed_edges = np.take(sig, [0, -1], axis=0)

        assert np.array_equal(original_edges, reconstructed_edges)

        return None

    def test_compress_tsc_float_num_indices_to_keep_warning(self):
        """
        Make sure we raise a single warning when we do ``num_indices_to_keep`` as a float that leads to less than 2
        points. Then confirm we keep the 2 edges we expected
        """
        with pytest.warns() as record:
            sig = compress_tsc(self.signal, num_indices_to_keep=1e-6)

        assert len(record) == 1, \
            f"Expected 1 warning for edge angle, got:\n{[i.message.args for i in record]}"

        original_edges = np.take(self.signal, [0, -1], axis=0)
        reconstructed_edges = sig.copy()

        assert np.array_equal(original_edges, reconstructed_edges)

        assert sig.shape == (2, 2)

        return None

    def test_compress_tsc_percentage(self):
        """
        Make sure ``tsc.reconstruct_signal()`` can return a percentage of values as expected
        """
        np.random.seed(0)
        signal_length = 100
        sig = np.c_[np.arange(signal_length), np.random.random(signal_length)]
        reconstruction = compress_tsc(sig, num_indices_to_keep=0.25)
        assert reconstruction.shape[0] / signal_length == 0.25
        return None

    def test_compress_tsc_persistence_cutoff(self, pers_cutoff: int = 1):
        """
        Make sure using the ``persistence_cutoff`` parameter behaves as expected.
        """

        sig_new = compress_tsc(self.signal, persistence_cutoff=pers_cutoff)

        # make sure we shrunk the signal
        assert sig_new.shape[0] < self.signal.shape[0]

        # excluding our keeping of the endpoints, there should be no persistence values > pers_cutoff
        new_pers = signal_persistence(sig_new)

        # drop pers value that starts / ends with starting point
        new_pers = new_pers.loc[~(new_pers.birth_index == 0), :]
        new_pers = new_pers.loc[~(new_pers.death_index == 0), :]

        # drop pers value that starts / ends with ending point
        new_pers = new_pers.loc[~(new_pers.birth_index == self.signal[-1, 0]), :]
        new_pers = new_pers.loc[~(new_pers.death_index == self.signal[-1, 0]), :]

        # all remaining persistence values should be above the cutoff
        assert new_pers.pers.min() > pers_cutoff

        # (and there were values below the cutoff originally)
        assert self.pers.pers.min() < pers_cutoff

        return None

    def test_compress_tsc_pers_diag(self):
        """
        Make sure using ``pers_diag`` as a dataframe or ``None`` gives the same result.
        """

        pers_cutoff = 0.5

        sig_0 = compress_tsc(self.signal, pers_diag=None, persistence_cutoff=pers_cutoff)
        sig_1 = compress_tsc(self.signal, pers_diag=self.pers, persistence_cutoff=pers_cutoff)

        assert np.array_equal(sig_0, sig_1)

        return None


class TestReconstructTSC:
    """
    Tests for ``tsc.reconstruct_tsc()``.
    """

    @pytest.fixture(autouse=True)
    def setup(self):
        np.random.seed(27703)

        # reconstruction will be 0 in between if no ``x_values``
        self.data = [-1, 1, -1, 1]

        self.times = [0, 2, 4, 6]

        # times when signal should interpolate to 0
        self.zero_value_times = [1, 3, 5]

        self.signal = np.c_[self.times, self.data]

    def teardown(self):
        pass

    def test_reconstruct_tsc_no_x_values(self):
        """
        Make sure ``tsc.reconstruct_tsc()`` gives us what we expect *without* ``x_values`` specified for a toy example.
        """

        out = reconstruct_tsc(self.signal, x_values=None)

        # expecting zeros in between each value
        expected_out = np.zeros(max(self.times)+1)
        expected_out[::2] = self.data

        assert np.array_equal(out, np.c_[np.arange(max(self.times) + 1), expected_out])

    def test_reconstruct_tsc_with_original_x_values(self):
        """
        Make sure ``tsc.reconstruct_tsc()`` gives us what we expect *with* original ``x_values``
        specified for a toy example.
        """

        out = reconstruct_tsc(self.signal, x_values=self.times)

        # expecting original values
        assert np.array_equal(out, self.signal)

    def test_reconstruct_tsc_with_different_x_values(self):
        """
        Make sure ``tsc.reconstruct_tsc()`` gives us what we expect *with* different ``x_values``
        specified for a toy example.
        """

        out = reconstruct_tsc(self.signal, x_values=self.zero_value_times)

        # expecting zero values
        assert np.array_equal(out, np.c_[self.zero_value_times, np.zeros(len(self.zero_value_times))])


@pytest.fixture(scope="class")
def init_pipeline_outputs(request):
    request.cls.test_input = np.array([1, -1, 5, -3, -2])
    request.cls.tsc_result = np.array([1, 3, 5, -3, -2])
    yield


@pytest.mark.usefixtures("init_pipeline_outputs")
class TestPipelineFunctionsSpecificValues:
    """
    Tests for the more interpretable pipeline functions on getting some specific values out.
    """

    @pytest.mark.parametrize("n_keep", [4, 0.8])
    def test_tsc_pipeline(self, n_keep):
        """
        Make sure ``tsc.tsc_pipeline()`` gives us the expected results for a toy example.
        """
        assert np.all(tsc_pipeline(signal=self.test_input, n_keep=n_keep) == self.tsc_result)

    def test_tsc_no_change(self):
        """
        Make sure ``tsc.tsc_pipeline()`` doesn't do anything if our request doesn't require any compression.
        """

        assert np.all(tsc_pipeline(signal=self.test_input, n_keep=np.inf) == self.test_input)
