# Gary Koplik
# gary<dot>koplik<at>geomdata<dot>com
# April, 2022
# test_utils.py


"""
Tests for ``tsc.utils.__init__.py``.
"""


import os
from scipy.io.wavfile import write
from tsc.utils.helper_functions import *


def test_min_max_normalize():
    """
    Make sure ``tsc.utils.min_max_normalize()`` behaves as expected.
    """

    arr = np.arange(-5, 5)

    new_arr = min_max_normalize(arr)

    # types should be float32
    #  (used to make all types consistent among compressions when checking compression size on disk)
    assert isinstance(new_arr[0], np.float32)

    # should be normalized into [0, 1]
    assert arr.min() < 0 and new_arr.min() == 0
    assert arr.max() > 1 and new_arr.max() == 1


def test_pydub_to_numpy():
    """
    Make sure ``tsc.utils.pydub_to_numpy()`` behaves as expected.
    """

    np.random.seed(0)
    size = 1000
    arr = min_max_normalize(np.random.random(size=size))

    # build out wav file to read in as audiosegment
    f = os.path.join("data/pytest", f"test_pydub_to_numpy.wav")
    write(f, rate=size, data=arr)
    audiosegment = AudioSegment.from_wav(f)

    # kill the file on disk before checking anything once it's in memory
    os.remove(f)

    # normalize to keep things consistent
    out_arr = min_max_normalize(pydub_to_numpy(audiosegment=audiosegment))
    assert out_arr.shape == arr.shape
    assert np.allclose(arr, out_arr, rtol=0.05)
