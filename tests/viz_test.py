# Gary Koplik
# gary<dot>koplik<at>geomdata<dot>com
# April, 2022
# viz_test.py

"""
Tests for ``tsc.viz.py``
"""

import numpy as np
from tsc import signal_persistence
from tsc.utils.viz import *


def test_plot_persistence():
    """
    Make sure ``tsc.viz.plot_persistence()`` behaves as expected.
    """

    data = np.array([
        [0., 5.],
        [1., 4.],
        [2.5, 2.5],
        [4.5, 4.5],
        [6.5, 3.],
        [9.5, 6.],
        [11.5, 11.],
        [13.5, 1.]
    ])

    pers = signal_persistence(data)

    try:
        fig, ax = plot_persistence(pers_data=pers, bounds=(0, 12), figsize=(10, 10))
        plot_persistence(pers_data=pers, bounds=None, fig=fig, ax=ax,
                         birth_death_line_kwargs=dict(c="green", ls="--"), c="blue", s=20)
        assert True
    except:
        assert False, \
            "No failures of any kind should have happened from those calls..."

    # should be able to handle empty df with pre-specified bounds
    df = pd.DataFrame()
    try:
        plot_persistence(pers_data=df, bounds=(0, 5))
        assert True
    except:
        assert False, \
            "Failed to handle empty dataframe with bounds"

    # cannot hand function only one of fig, ax
    try:
        plot_persistence(pers_data=pers, fig=fig, ax=None)
        assert False
    except ValueError:
        assert True
